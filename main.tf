locals {
  common_tags = merge(
    map("Environment", var.environment),
    map("Provisioner", "terraform")
  )
}

resource "aws_launch_template" "this" {
  name_prefix   = format("%s-launch-template-", var.asg_name)
  image_id      = var.asg_image_id
  instance_type = var.asg_instance_type
  iam_instance_profile {
    name = var.asg_instance_profile_name
  }

  key_name               = var.asg_key_name
  vpc_security_group_ids = var.asg_security_groups

  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      volume_size = var.asg_volume_size
    }
  }

  tags = merge(
    map("Name", format("%s-launch-template", var.asg_name)),
    local.common_tags
  )

  tag_specifications {
    resource_type = "instance"

    tags = merge(
      map("Name", var.asg_name),
      local.common_tags
    )
  }

  tag_specifications {
    resource_type = "volume"
    tags = merge(
      map("Name", format("%s-vol", var.asg_name)),
      local.common_tags
    )
  }
}

resource "aws_autoscaling_group" "this" {
  name_prefix               = var.asg_name
  max_size                  = var.asg_max_size
  min_size                  = var.asg_min_size
  default_cooldown          = var.asg_default_cooldown
  health_check_grace_period = var.asg_health_check_grace_period
  # assuming currently only support EC2
  health_check_type    = "EC2"
  vpc_zone_identifier  = var.asg_subnet_ids
  termination_policies = var.asg_termination_policies

  launch_template {
    id = aws_launch_template.this.id
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = concat(
    list(
      {
        key                 = "Name"
        value               = var.asg_name
        propagate_at_launch = true
      },
      {
        key                 = "Environment"
        value               = var.environment
        propagate_at_launch = true
      },
      {
        key                 = "Provisioner"
        value               = "terraform"
        propagate_at_launch = true
      }
    )
  )
}
