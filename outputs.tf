output "asg_name" {
  value = aws_autoscaling_group.this.name
}

output "asg_max_size" {
  value = var.asg_max_size
}

output "asg_min_size" {
  value = var.asg_min_size
}

output "asg_launch_template_name" {
  value = aws_launch_template.this.name
}

output "asg_launch_template_id" {
  value = aws_launch_template.this.id
}

output "asg_subnet_ids" {
  value = var.asg_subnet_ids
}